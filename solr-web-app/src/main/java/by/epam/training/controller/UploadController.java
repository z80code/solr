package by.epam.training.controller;

import by.epam.training.entity.UploadResult;
import by.epam.training.extractor.IFieldsExtractor;
import by.epam.training.extractor.exception.FieldsExtractorException;
import by.epam.training.service.ISolrService;
import by.epam.training.storage.IFileStorage;
import by.epam.training.storage.exception.FileStorageException;
import org.apache.log4j.Logger;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Upload controller.
 */
@Controller
public class UploadController {

	private Logger logger = Logger.getLogger(UploadController.class);

	private ISolrService<SolrInputDocument> solrService;

	private IFileStorage fileStorage;

	private IFieldsExtractor<SolrInputDocument> fieldsExtractor;

	@Autowired
	public UploadController(ISolrService<SolrInputDocument> solrService,
	                        IFileStorage fileStorage,
	                        IFieldsExtractor<SolrInputDocument> fieldsExtractor) {
		this.solrService = solrService;
		this.fileStorage = fileStorage;
		this.fieldsExtractor = fieldsExtractor;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public ResponseEntity uploadingPost(
			@RequestParam("file") List<MultipartFile> uploadingFiles,
			MultipartRequest request) {
		logger.info("Getting files from user");
		List<SolrInputDocument> addedBookDocumentList = new ArrayList<>();
		List<String> failedBookDocumentFileNamesList = new ArrayList<>();
		try {
			for (MultipartFile uploadedFile : uploadingFiles) {
				File file;
				try {
					file = fileStorage.store(uploadedFile);
					SolrInputDocument solrInputDocument = fieldsExtractor.extractFromFictionBook(file);
					addedBookDocumentList.add(solrInputDocument);

				} catch (FileStorageException | FieldsExtractorException e) {
					logger.warn("Cant upload file with name: \"" +
							uploadedFile.getOriginalFilename() +
							"\" Cause: " + e.getMessage(),e);
					failedBookDocumentFileNamesList.add(uploadedFile.getOriginalFilename());
				}
			}
			logger.info("Index to solr");
			solrService.index(addedBookDocumentList);
			logger.info("Indexing complete success.");
			return ResponseEntity.ok(
					new UploadResult(
							// "id" default name for key(it is filename). Creating list of names for success uploaded files"
							addedBookDocumentList.stream().map(doc -> doc.get("id").getValue().toString()).collect(Collectors.toList()),
							failedBookDocumentFileNamesList,
							"success",
							failedBookDocumentFileNamesList.size() > 0 ? "Some files not been uploaded" : null
					));

		} catch (Exception e) {
			logger.error("Uploading failure with cause: "+ e.getMessage());
			// TODO separate exceptions
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		} finally {
			logger.info("Response was send to user.");
		}
	}
}
