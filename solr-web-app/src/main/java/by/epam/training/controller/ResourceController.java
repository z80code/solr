package by.epam.training.controller;

import by.epam.training.storage.IFileStorage;
import by.epam.training.storage.exception.FileStorageException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.File;
import java.io.IOException;

@Controller
public class ResourceController {

   Logger logger = LoggerFactory.getLogger(ResourceController.class);

   private IFileStorage fileStorage;

   @Autowired
   public ResourceController(IFileStorage fileStorage) {
      this.fileStorage = fileStorage;
   }

   @GetMapping(value = "/image/{filename}")
   public ResponseEntity getImage(@PathVariable String filename) {
      logger.info("Getting resource with name: " + filename);
      try {
         String imageBase64 = fileStorage.getImageBase64ByName(filename);
         return ResponseEntity.ok(imageBase64);
      } catch (FileStorageException e) {
         logger.error("Getting resource(image) was failure for name: " + filename);
         return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
      }
   }

   @GetMapping(value = "/file/{filename}")
   public ResponseEntity getFile(@PathVariable String filename) {
      logger.info("Getting resource with name: " + filename);
      try {
         File file = fileStorage.getByNameFile(filename);
         return ResponseEntity.ok(String.join("", FileUtils.readLines(file)));
      } catch (FileStorageException | IOException e) {
         logger.error("Getting resource(file) was failure for name: " + filename);
         return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
      }
   }
}
