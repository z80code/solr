package by.epam.training.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Not used in my task.
 * Example only.
 */
@Controller
public class SearchController {
	@PostMapping(value = "/search")
	public ResponseEntity search(@RequestBody String searchString) {
		ResponseEntity responseEntity = ResponseEntity.ok(searchString);
		System.out.println(searchString);
		return responseEntity;
	}
}
