package by.epam.training.entity;

import org.apache.solr.client.solrj.beans.Field;
import org.springframework.data.annotation.Id;
import org.springframework.data.solr.core.mapping.SolrDocument;

import java.util.Date;
import java.util.List;

@SolrDocument(solrCoreName = "library")
public class BookDocument {

	@Id
	@Field
	private String id;

	@Field
	private String title;

	@Field
	private String image;

	@Field
	private List<String> authors;

	@Field
	private String publisher;

	@Field
	private String isdn;

	@Field
	private Date creationDate;

	@Field
	private Date publicationDate;

	@Field
	private Date uploadDate; //(generate during indexation),

	@Field
	private String annotation;

	@Field
	private List<String> sections; //contents of the book itself (per page).

	@Field
	private String document;

	public BookDocument() {
	}

	public BookDocument(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}


	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public String getIsdn() {
		return isdn;
	}

	public void setIsdn(String isdn) {
		this.isdn = isdn;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public List<String> getSections() {
		return sections;
	}

	public void setSections(List<String> sections) {
		this.sections = sections;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public BookDocument addTitle(String title) {
		this.title = title;
		return this;
	}

	public BookDocument addImage(String image) {
		this.image = image;
		return this;
	}

	public BookDocument addAuthors(List<String> authors) {
		this.authors = authors;
		return this;
	}

	public BookDocument addPublisher(String publisher) {
		this.publisher = publisher;
		return this;
	}

	public BookDocument addIsdn(String isdn) {
		this.isdn = isdn;
		return this;
	}

	public BookDocument addCreationDate(Date creationDate) {
		this.creationDate = creationDate;
		return this;
	}

	public BookDocument addPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
		return this;
	}

	public BookDocument addUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
		return this;
	}

	public BookDocument addAnnotation(String annotation) {
		this.annotation = annotation;
		return this;
	}

	public BookDocument addSections(List<String> sections) {
		this.sections = sections;
		return this;
	}

	public BookDocument addDocument(String document) {
		this.document = document;
		return this;
	}

	@Override
	public String toString() {
		return "BookDocument{" +
			 "id='" + id + '\'' +
			 ", title='" + title + '\'' +
			 ", image='" + image + '\'' +
			 ", authors=" + authors +
			 ", publisher=" + publisher +
			 ", isdn='" + isdn + '\'' +
			 ", creationDate=" + creationDate +
			 ", publicationDate=" + publicationDate +
			 ", uploadDate=" + uploadDate +
			 ", annotation='" + annotation + '\'' +
			 ", sections=" + sections +
			 ", document='" + document + '\'' +
			 '}';
	}
}
