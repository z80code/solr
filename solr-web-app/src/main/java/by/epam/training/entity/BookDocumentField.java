package by.epam.training.entity;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum BookDocumentField {

    ID("id"),
    TITLE("title"),
    GENRES("genres"),
    AUTHORS("authors"),
    IMAGE("image"),
    PUBLISHER("publisher"),
    ISDN("isdn"),
    CREATION_DATE("creationDate"),
    PUBLICATION_DATE("publicationDate"),
    UPLOAD_DATE("uploadDate"),
    ANNOTATION("annotation"),
    CHAPTERS("chapters"),
    DOCUMENT_STORED_NAME("documentStoredName");

    private String value;
    BookDocumentField(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public List<String> getAsList() {
        return Arrays.stream(BookDocumentField.values()).map(item -> item.value).collect(Collectors.toList());
    }
}
