package by.epam.training.entity;

import java.util.List;

public class UploadResult {
	private List<String> uploadedNameList;
	private List<String> failedNameList;

	private String status;

	private String errorMessage;

	public UploadResult() {
	}

	public UploadResult(List<String> uploadedNameList, List<String> failedNameList, String status, String errorMessage) {
		this.uploadedNameList = uploadedNameList;
		this.failedNameList = failedNameList;
		this.status = status;
		this.errorMessage = errorMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getUploadedNameList() {
		return uploadedNameList;
	}

	public void setUploadedNameList(List<String> uploadedNameList) {
		this.uploadedNameList = uploadedNameList;
	}

	public List<String> getFailedNameList() {
		return failedNameList;
	}

	public void setFailedNameList(List<String> failedNameList) {
		this.failedNameList = failedNameList;
	}
}
