package by.epam.training.extractor.exception;

public class FieldsExtractorException extends Exception {
	public FieldsExtractorException() {
	}

	public FieldsExtractorException(String message) {
		super(message);
	}

	public FieldsExtractorException(String message, Throwable cause) {
		super(message, cause);
	}
}
