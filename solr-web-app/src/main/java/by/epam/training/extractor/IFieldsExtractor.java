package by.epam.training.extractor;

import by.epam.training.entity.BookDocument;
import by.epam.training.extractor.exception.FieldsExtractorException;

import java.io.File;

public interface IFieldsExtractor<T> {
	T extractFromFictionBook(File file) throws FieldsExtractorException;
}
