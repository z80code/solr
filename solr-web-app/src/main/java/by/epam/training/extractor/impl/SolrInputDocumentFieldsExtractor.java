package by.epam.training.extractor.impl;

import by.epam.training.entity.BookDocumentField;
import by.epam.training.extractor.IFieldsExtractor;
import by.epam.training.extractor.exception.FieldsExtractorException;
import by.epam.training.fb2.FBDocument;
import by.epam.training.fb2.exception.FBException;
import by.epam.training.storage.IFileStorage;
import by.epam.training.storage.exception.FileStorageException;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class SolrInputDocumentFieldsExtractor implements IFieldsExtractor<SolrInputDocument> {

   private static IFileStorage storage;

   @Autowired
   public SolrInputDocumentFieldsExtractor(IFileStorage storage) {
      this.storage = storage;
   }

   private static void saveImage(String filename, String content) throws FileStorageException {
      storage.storeImageBase64(filename, content);
   }

   @Override
   public SolrInputDocument extractFromFictionBook(File file) throws FieldsExtractorException {
      try {
         return buildBookDocument(file);
      } catch (FileNotFoundException e) {
         throw new FieldsExtractorException("File not found.");
      } catch (IOException e) {
         throw new FieldsExtractorException(e.getMessage(), e);
      } catch (SAXException | ParserConfigurationException | FBException e) {
         throw new FieldsExtractorException("Bad file format.");
      } catch (FileStorageException e) {
         throw new FieldsExtractorException("Can`n to storage image.");
      }
   }

   private static String EMPTY_STRING = "";

   private static String emptyInstead(String string) {
      return string == null ? EMPTY_STRING : string;
   }

   private SolrInputDocument buildBookDocument(File file) throws IOException, SAXException, ParserConfigurationException, FileStorageException, FBException {

      FBDocument doc = new FBDocument(file);

      SolrInputDocument solrInputDocument = new SolrInputDocument();

      // id
      String id = file.getName();
      solrInputDocument.addField(BookDocumentField.ID.toString(), id);

      // title
      solrInputDocument.addField(BookDocumentField.TITLE.toString(), emptyInstead(doc.getBookTitle()));

      // genre
      solrInputDocument.addField(BookDocumentField.GENRES.toString(), doc.getBookGenres());

      // Extract and store cover image (optional but preferred)
      String resourceName = file.getName();
      solrInputDocument.addField(BookDocumentField.IMAGE.toString(),
              InnerHelper.extractImage(doc, resourceName.replace(".fb2", "")));
      // authors;
      solrInputDocument.addField(
              BookDocumentField.AUTHORS.toString(),
              new ArrayList<String>() {{
                 add(doc.getBookAuthors());
              }});

      // publisher;
      solrInputDocument.addField(BookDocumentField.PUBLISHER.toString(), emptyInstead(doc.getBookPublisher()));

      // isdn;
      solrInputDocument.addField(BookDocumentField.ISDN.toString(), emptyInstead(doc.getBookIsbn()));

      // Date creationDate
      solrInputDocument.addField(BookDocumentField.CREATION_DATE.toString(),
              InnerHelper.parseCreationDate(doc.getBookCreationDate()));

      //	Date publicationDate;
      solrInputDocument.addField(BookDocumentField.PUBLICATION_DATE.toString(), InnerHelper.parsePublicationDate(doc.getBookPublicationDate()));

      // uploadDate (generate during indexation)
      solrInputDocument.addField(BookDocumentField.UPLOAD_DATE.toString(), InnerHelper.extractUploadDate(file));

      // annotation
      solrInputDocument.addField(BookDocumentField.ANNOTATION.toString(), emptyInstead(doc.getBookAnnotation()));

      //contents of the book itself (per page).
      solrInputDocument.addField(BookDocumentField.CHAPTERS.toString(), doc.getBookChapters());

      // String document;
      solrInputDocument.addField(BookDocumentField.DOCUMENT_STORED_NAME.toString(), InnerHelper.extractDocumentStoreName(resourceName));

      return solrInputDocument;
   }

   private static class InnerHelper {

      private static List<SimpleDateFormat> DATE_PATTERNS = new ArrayList<SimpleDateFormat>() {
         {
            add(new SimpleDateFormat("yyyy-MM-dd"));
            add(new SimpleDateFormat("dd.MM.yyyy"));
         }
      };

      private static Date parseDate(String dateString) {
         for (SimpleDateFormat pattern : DATE_PATTERNS) {
            try {
               return pattern.parse(dateString);
            } catch (ParseException pe) {
               // TODO log on failure
            }
         }
         return new Date(0);
      }

      /**
       * Parse image
       */
      static String extractImage(FBDocument doc, String resourceName) throws FileStorageException {
         try {
            saveImage(resourceName, doc.getBookCoverPageImage());
            return resourceName;
         } catch (FBException e) {
            return "";
         }
      }

      /**
       * Parse Date creationDate
       */
      static Date parseCreationDate(String date) {
         try {
            return parseDate(date);
         } catch (NullPointerException e) {
            return new Date(0);
         }
      }

      /**
       * Parse Date publicationDate
       */
      static Date parsePublicationDate(String date) {
         final String dateTemplate = "yyyy";
         try {
            return new SimpleDateFormat(dateTemplate).parse(date);
         } catch (ParseException e) {
            return new Date(0);
         }
      }

      /**
       * parse Date uploadDate
       */
      static Date extractUploadDate(File file) {
         long time = file.lastModified();
         return new Date(time);
      }

      /**
       * Parse documentStoreName
       */
      static String extractDocumentStoreName(String resourceName) {
         return resourceName;
      }

   }
}
