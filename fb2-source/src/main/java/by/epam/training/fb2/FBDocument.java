/*
 *  Fiction Book Tools.
 *  Copyright (C) 2007  Denis Nelubin aka Gelin
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  http://gelin.ru/project/fictionbook/
 *  mailto:den@gelin.ru
 */

package by.epam.training.fb2;

import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipFile;
import java.util.zip.ZipEntry;
import java.util.zip.GZIPInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import by.epam.training.fb2.exception.FBException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.XPath;
import org.dom4j.Node;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.dom4j.rule.Pattern;

/**
 * Document which represents Fiction Book.
 */
public class FBDocument {

   /**
    * commons logging instance
    */
   protected Log log = LogFactory.getLog(this.getClass());

   /**
    * dom4j document instance
    */
   Document dom;

   /**
    * document factory which is used to create dom4j document
    */
   DocumentFactory factory;

   XPath bookTitleXPath;

   /**
    * XPath expression to detect inline elements
    */
   XPath inlinesXPath;

   public static final Map NS_URIS;

   static {
      Map NSUris = new HashMap();
      NSUris.put("fb", "http://www.gribuser.ru/xml/fictionbook/2.0");
      NSUris.put("fbg", "http://www.gribuser.ru/xml/fictionbook/2.0/genres");
      NSUris.put("l", "http://www.w3.org/1999/xlink");
      NS_URIS = Collections.unmodifiableMap(NSUris);
   }

   /**
    * Constructor from file.
    *
    * @param file file with Fiction Book document
    * @throws FBException if any error occured while reading document,
    *                     cause of error is wrapped.
    */
   public FBDocument(File file) throws FBException {
      factory = new DocumentFactory();
      factory.setXPathNamespaceURIs(NS_URIS); //for correct working of XPath for elements
      if (log.isInfoEnabled()) {
         log.info("creating document from file " + file);
      }
      try {
         if (file.getName().endsWith(".zip")) {
            dom = readZip(file);
         } else if (file.getName().endsWith(".gz")) {
            dom = readGZip(file);
         }
         if (dom == null) {
            dom = read(file);
         }
         prepareXPaths();
      } catch (Exception e) {
         String err = "can't create document from file " + file;
         log.error(err, e);
         throw new FBException(err, e);
      }
   }

   /**
    * Returns internal dom4j document.
    */
   public Document getDocument() {
      return dom;
   }

   /**
    * Creates new XPath expression with namespace prafixes set to
    * canonical values.
    * These prefixes should be used in created expression:<br/>
    * fb - http://www.gribuser.ru/xml/fictionbook/2.0<br/>
    * fbg - http://www.gribuser.ru/xml/fictionbook/2.0/genres<br/>
    * l - http://www.w3.org/1999/xlink<br/>
    */
   public XPath createXPath(String xpathExpression) {
      XPath result = factory.createXPath(xpathExpression);
      //result.setNamespaceURIs(NS_URIS); //defined in factory
      return result;
   }

   /**
    * Creates new XPath pattern for rule processing.
    */
   public Pattern createPattern(String xpathPattern) {
      return factory.createPattern(xpathPattern);
   }

   /**
    * Returns value of /FictionBook/description/title-info/book-title
    * element.
    */
   public String getBookTitle() {
      return bookTitleXPath.valueOf(dom);
   }

   /**
    * Returns value of /FictionBook/description/title-info/genre
    * element.
    */
   public List<String> getBookGenres() {
      XPath bookAuthorXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:genre");
      return getValues(bookAuthorXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/authors
    * element.
    */
   public String getBookAuthors() {
      XPath bookAuthorFirstNameXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:author/fb:first-name");
      XPath bookAuthorLastNameXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:author/fb:last-name");
      return getValue(bookAuthorFirstNameXPath) + " " + getValue(bookAuthorLastNameXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/publisher
    * element.
    */
   public String getBookPublisher() {
      XPath bookPublisherXPath =
              createXPath("/fb:FictionBook/fb:description/fb:publish-info/fb:publisher");
      return getValue(bookPublisherXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/isdn
    * element.
    */
   public String getBookIsbn() {
      XPath bookIsbnXPath =
              createXPath("/fb:FictionBook/fb:description/fb:publish-info/fb:isbn");
      return getValue(bookIsbnXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/creationDate
    * element.
    */
   public String getBookCreationDate() {
      XPath bookCreationDateXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:date");
      return getValue(bookCreationDateXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/publicationDate
    * element.
    */
   public String getBookPublicationDate() {
      XPath bookPublicationDateXPath =
              createXPath("c/fb:FictionBook/fb:description/fb:publish-info/fb:year");
      return getValue(bookPublicationDateXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/annotation
    * element.
    */
   public String getBookAnnotation() {
      XPath bookAnnotationXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:annotation");
      return getValue(bookAnnotationXPath);
   }

   /**
    * Returns value of /FictionBook/description/title-info/chapters
    * element.
    */
   public List<String> getBookChapters() {
      final String sectionPath = "/fb:FictionBook/fb:body/fb:section";

      XPath bookChaptersXPath = createXPath(sectionPath);
      List<Node> nodes = bookChaptersXPath.selectNodes(dom);

      List<String> result = new ArrayList<>();

      for (Node node : nodes) {
         StringBuilder sb = new StringBuilder();

         sb.append(node.asXML());

//  sb.append(String.join(" ", node.selectNodes("./descendant-or-self::fb:*/*[string-length(text()) > 0]/text()").stream()
//    .map(innerNode -> innerNode.getText().trim()).collect(Collectors.toList())));
         String line = sb.toString().trim();

         if (line.length() > 0) {
            result.add(line);
         }
      }
      return result;
   }

   private String getInnerParagraphAsString(Node node) {
      List<Node> nodes = node.selectNodes("/fb:p");

      return String.join(" ", nodes
              .stream()
              .map(innerNode -> innerNode.getText().trim()).collect(Collectors.toList()));
   }

   /**
    * Returns value of /FictionBook/description/title-info/coverPage
    * element.
    */
   public String getBookCoverPageImage() throws FBException {
      XPath bookIdCoverPageImageXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:coverpage/fb:image/@l:href");
      return getImage(getValue(bookIdCoverPageImageXPath));
   }


   private String getValue(XPath xPath) {
      return xPath.valueOf(dom).trim();
   }

   private List<String> getValues(XPath xPath) {
      return xPath.selectNodes(dom).stream().map(node -> node.getText().trim()).collect(Collectors.toList());
   }

   /**
    * Returns <code>true</code> if specified DOM Node is "inline" node
    * (i.e. not forms new paragraph).
    */
   public boolean isInline(Node node) {
      return inlinesXPath.matches(node);
   }

   /**
    * Gets an image, embedded into the document.
    *
    * @param href link to the image, "#cover.jpg" for example
    * @return image as an icon
    * @throws FBException if no such image
    */
   public String getImage(String href) throws FBException {
      if (!href.startsWith("#")) {
         throw new FBException("only local images are supported");
      }
      String id = href.substring(1);
      Node node = dom.selectSingleNode("//fb:binary[@id='" + id + "']");
      if (node == null) {
         throw new FBException("no binary with id = '" + id + "'");
      }
      try {
         return node.getText();
      } catch (Exception e) {
         throw new FBException("error occurred while reading image", e);
      }
   }

   String getXMLEncoding() {
      if (dom.getXMLEncoding() == null)
         return "ISO-8859-1";
      return dom.getXMLEncoding();
   }

   void prepareXPaths() {
      bookTitleXPath =
              createXPath("/fb:FictionBook/fb:description/fb:title-info/fb:book-title");
      inlinesXPath = createXPath(
              "//fb:empty-line|" +
                      "//fb:strong|//fb:emphasis|" +
                      "//fb:sub|//fb:sup|" +
                      "//fb:strikethrough|" +
                      "//fb:code|" +
                      "//fb:a");
   }

   SAXReader getSAXReader() {
      return new SAXReader(factory);
   }

   /**
    * Read Fiction Book from zip archive.
    */
   Document readZip(File file) throws IOException, DocumentException {
      Document result = null;
      ZipFile zipFile = new ZipFile(file);
      Enumeration<? extends ZipEntry> entries = zipFile.entries();
      while (entries.hasMoreElements()) {
         ZipEntry entry = entries.nextElement();
         String name = entry.getName();
         if (file.getName().startsWith(name) ||
                 name.endsWith(".fb2")) {
            if (result != null) {
               log.warn("Zip file " + file +
                       " contains multiple Fiction Book documents");
               break;
            }
            if (log.isInfoEnabled()) {
               log.info("unzipping " + name);
            }
            SAXReader xmlReader = getSAXReader();
            result = xmlReader.read(zipFile.getInputStream(entry));
         }
      }
      return result;
   }

   /**
    * Read GZipped Fiction Book.
    */
   Document readGZip(File file) throws IOException, DocumentException {
      if (log.isInfoEnabled()) {
         log.info("ungzipping " + file);
      }
      SAXReader xmlReader = getSAXReader();
      return xmlReader.read(new GZIPInputStream(new FileInputStream(file)));
   }

   /**
    * Read not compressed Fiction Book.
    */
   Document read(File file) throws IOException, DocumentException {
      SAXReader xmlReader = getSAXReader();
      return xmlReader.read(file);
   }

}