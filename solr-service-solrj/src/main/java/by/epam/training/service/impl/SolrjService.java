package by.epam.training.service.impl;

import by.epam.training.service.ISolrService;
import by.epam.training.service.exception.SolrServiceException;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.MapSolrParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class SolrjService implements ISolrService<SolrInputDocument> {

   private Logger logger = LoggerFactory.getLogger(SolrjService.class);

   private static final String ID_KEY = "id";

   private final SolrClient solrClient;

   @Autowired
   public SolrjService(SolrClient solrClient) {
      this.solrClient = solrClient;
   }

   /**
    * This method not used in my decision. It`s for me just a template.
    *
    * @param string String for searching
    * @throws SolrServiceException Exception wrapper for underlayered exceptions
    */
   @Override
   public void search(String string) throws SolrServiceException {
		final Map<String, String> queryParamMap = new HashMap<String, String>();
		queryParamMap.put("q", string);
		queryParamMap.put("fl", "id, name");
		MapSolrParams queryParams = new MapSolrParams(queryParamMap);
		try {
			final QueryResponse response = solrClient.query(queryParams);
			final SolrDocumentList documents = response.getResults();
			//TODO some handle code
		} catch (SolrServerException | IOException e) {
			throw new SolrServiceException(e.getMessage(), e);
		}
   }

   @Override
   public void index(Collection<SolrInputDocument> books) throws SolrServiceException {
      logger.info("Start index of files. Count: " + books.size());
      //	TODO answer with not indexed files
      // Map<SolrInputDocument, UpdateResponse> responseMap = new LinkedHashMap<>();
      try {
         for (SolrInputDocument book : books) {
            logger.info("Indexing: " + book.get(ID_KEY).getValue());
            try {
               UpdateResponse response = solrClient.add(books);
               logger.info("Done file [time: " + response.getQTime() + "] : " + book.get(ID_KEY).getValue());
               //	responseMap.put(book, response);
            } catch (IOException | SolrServerException e) {
               logger.warn("Indexing was failure with file: " + book.get(ID_KEY).getValue(), e);
               System.out.println(e);
            }
         }
         logger.info("Commit changes.");
         solrClient.commit();
         logger.info("Indexing success.");
      } catch (SolrServerException | IOException e) {
         logger.error("Indexing was failure.", e);
         throw new SolrServiceException(e.getMessage(), e);
      } catch (Exception e) {
         logger.error("Solr server error.", e);
         throw new SolrServiceException(e.getMessage(), e);
      }
   }
}
