package by.epam.training.config;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("classpath:/application.properties")
public class SolrAppConfiguration {

	@Value("${solr.core.library.url}")
	private String urlCoreLibrary;

	@Bean
	public SolrClient getSolrClient(){
		return new HttpSolrClient.Builder(urlCoreLibrary).build();
	}
}
