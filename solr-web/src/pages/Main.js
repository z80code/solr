import React, {Component} from "react";

import PanelGroup from "react-bootstrap/es/PanelGroup";
import NavBar from "./component/NavBar";

import SearchApp from "./Search";
import Upload from "./Upload";
import Config from "./Config";
import Browse from "./Browse";

import {Page} from "../constants/constants"

export default class Main extends Component {
   constructor(props) {
      super(props);
      this.state = {
         selectedPage: Page.BROWSE,
      }
   }

   render() {
      let currentPage = Page.SEARCH;
      switch (this.state.selectedPage) {
         case Page.SEARCH: {
            currentPage = (
               <SearchApp/>
            );
            break;
         }
         case Page.UPLOAD: {
            currentPage = (
               <Upload/>
            );
            break;
         }
         case Page.CONFIG: {
            currentPage = (
               <Config/>
            );
            break;
         }
         case Page.BROWSE: {
            currentPage = (
               <Browse/>
            );
            break;
         }
         //    ... other pages ...
      }

      return (
         <div style={{marginTop: 50 + "px"}}>
            <NavBar onSelect={(eventKey) => {
               this.setState({
                  selectedPage: eventKey
               });
            }}/>
            <div className="container">
               <div className="pageBody">
                  <PanelGroup>
                     {/*page header*/}
                     {/*page body*/}
                     {currentPage}
                     {/*page footer*/}
                  </PanelGroup>
               </div>
            </div>
         </div>
      )
   }
}
