import React, {Component} from "react";

import {
   Grid
} from "material-ui";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import BrowseResult from "./component/BrowseResult";

const styles = {};

class Browse extends Component {

   constructor(props) {
      super(props);
      this.state = {}
   }

   render() {
      return (
         <div>
            <Grid container spacing={8}>
               <Grid item xs={12} style={{textAlign: "center"}}>
                  <h1>Browse</h1>
               </Grid>
            </Grid>
            <BrowseResult/>
         </div>
      );
   }
}

Browse.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Browse);