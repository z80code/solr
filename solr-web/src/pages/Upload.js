import React, {Component} from "react";

import {send} from "../actions/actions";
import {Button, Grid} from "material-ui";
import {FileUpload} from "material-ui-icons";

const filesPerRequest = 5;

export default class Upload extends Component {

   constructor(props) {
      super(props);

      this.onSubmit = this.onSubmit.bind(this);
      this.startIndex = 0;
      this.array = [];
      this.state = {
         filesInput: null
      }
   }

   onSubmit() {
      console.log(this.startIndex);
      if (this.state.filesInput === null) {
         console.log("Files not selected.");
         return;
      }
      if (this.startIndex === 0) {
         console.log(this.startIndex);
         this.array = [];
         const files = this.state.filesInput.files;
         for (let key in files) {
            if (files.hasOwnProperty(key) && files[key] instanceof File) {
               this.array.push(files[key]);
            }
         }
      }

      if ((this.startIndex + filesPerRequest) <= this.array.length) {
         let formData = new FormData();
         for (let i = 0; i < filesPerRequest; i++) {
            formData.append("file", this.array[this.startIndex + i]);
         }
         send(formData,
            (response) => {
               console.log(this.startIndex);
               this.startIndex += filesPerRequest;
               this.onSubmit();
            }
         )
      } else {
         let formData = new FormData();
         for (let i = 0; i < (this.array.length - this.startIndex) % filesPerRequest; i++) {
            formData.append("file", this.array[this.startIndex + i]);
         }
         send(formData,
            (response) => {
               this.setState({filesInput: null});
               this.startIndex = 0;
            }
         )
      }
   }

   render() {
      return (
         <div>
            <Grid container spacing={8}>
               <Grid item xs={12}>
                  <h1 style={{textAlign: "center"}}>Uploading</h1>
               </Grid>
               <form id="fileUpload" style={{width: "900px"}}>
                  <Grid item xs={4} sm={2} style={{float: "left"}}>
                     <label>Select files</label>
                     <input type="file"
                            className="form-control-file"
                            id="formControlFile"
                            accept=".fb2,.FB2"
                            multiple={true}
                            ref={(input) => {
                               this.state.filesInput = input;
                            }}
                     />
                  </Grid>
                  <Grid item xs={4} sm={2} style={{float: "right"}}>
                     <Button raised color="default" onClick={this.onSubmit}>
                        Upload
                     </Button>
                  </Grid>
                  <Grid item xs={4} sm={2} style={{float: "right", margin:"0 20px"}}>
                     <Button raised color="accent" type="reset">
                        Reset
                     </Button>
                  </Grid>

               </form>
            </Grid>
         </div>
      );
   }
}