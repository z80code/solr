import React, {Component} from "react";

import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import {MenuItem} from 'material-ui/Menu';
import {withStyles} from 'material-ui/styles';
import {getSuggest} from "../../actions/actions"
import Grid from 'material-ui/Grid';

function renderInput(inputProps) {
   const {classes, autoFocus, value, ref, ...other} = inputProps;

   return (
      <TextField
         autoFocus={autoFocus}
         className={classes.textField}
         value={value}
         inputRef={ref}
         InputProps={{
            classes: {
               input: classes.input,
            },
            ...other,
         }}
      />
   );
}

function renderSuggestion(suggestion, {query, isHighlighted}) {
   const matches = match(suggestion.label, query);
   const parts = parse(suggestion.label, matches);

   return (
      <MenuItem selected={isHighlighted} component="div">
         <div>
            {parts.map((part, index) => {
               return part.highlight ? (
                  <span key={String(index)} style={{fontWeight: "bold", fontSize: "1.5em"}}>
              {part.text}
            </span>
               ) : (
                  <strong key={String(index)} style={{fontWeight: "normal", fontSize: "1.5em"}}>
                     {part.text}
                  </strong>
               );
            })}
         </div>
      </MenuItem>
   );
}

function renderSuggestionsContainer(options) {
   const {containerProps, children} = options;

   return (
      <Paper {...containerProps} square  style={{zIndex: 1}}>
         {children}
      </Paper>
   );
}

function getSuggestionValue(suggestion) {
   return suggestion.label;
}

function getSuggestions(value, suggestionsArr) {
   const inputValue = value.trim().toLowerCase();
   const inputLength = inputValue.length;
   let count = 0;

   return inputLength === 0
      ? []
      : suggestionsArr.filter(suggestion => {
         const keep =
            count < 5 && suggestion.label.toLowerCase().slice(0, inputLength) === inputValue;

         if (keep) {
            count += 1;
         }
         return keep;
      });
}

const styles = theme => ({
   container: {
      flexGrow: 1,
      position: 'relative',
      height: 80,
   },
   suggestionsContainerOpen: {
      position: 'absolute',
      marginTop: theme.spacing.unit,
      marginBottom: theme.spacing.unit * 3,
      left: 0,
      right: 0,
   },
   suggestion: {
      display: 'block',
   },
   suggestionsList: {
      margin: 0,
      padding: 0,
      listStyleType: 'none',
   },
   textField: {
      width: '100%',
   },
   input: {
      fontSize: "2em"
   }
});

class IntegrationAutosuggest extends Component {

   constructor(props) {
      super(props);
      this.state = {
         value: '',
         suggestions: [],
      };
   }


   handleSuggestionsFetchRequested = ({value}) => {
      getSuggest(
         value,
         (result) => {
            let suggestions = result.suggest.libSuggester[value].suggestions
               .map(it => {
                  return {label: it.term.toLowerCase()};
               });
            console.log(suggestions);
            this.setState({
               suggestions: getSuggestions(value, suggestions)
            });
         }
      )

   };

   handleSuggestionsClearRequested = () => {
      this.setState({
         suggestions: [],
      });
   };

   handleChange = (event, {newValue}) => {
      this.props.valueUpdated(newValue);
      this.setState({
         value: newValue,
      });
   };

   render() {
      const {classes} = this.props;

      return (

         <Autosuggest
            theme={{
               container: classes.container,
               suggestionsContainerOpen: classes.suggestionsContainerOpen,
               suggestionsList: classes.suggestionsList,
               suggestion: classes.suggestion,
            }}
            renderInputComponent={renderInput}
            suggestions={this.state.suggestions}
            onSuggestionsFetchRequested={this.handleSuggestionsFetchRequested}
            onSuggestionsClearRequested={this.handleSuggestionsClearRequested}
            renderSuggestionsContainer={renderSuggestionsContainer}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            inputProps={{
               autoFocus: true,
               classes,
               placeholder: 'Search...',
               value: this.state.value,
               onChange: this.handleChange,
            }}
         />
      );
   }
}

IntegrationAutosuggest.propTypes = {
   classes: PropTypes.object.isRequired,
   valueUpdated: PropTypes.func.isRequired
};

export default withStyles(styles)(IntegrationAutosuggest);