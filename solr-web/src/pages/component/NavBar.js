import React, {Component} from 'react'
import {Nav, Navbar, NavItem} from "react-bootstrap";

import {Page} from "../../constants/constants";

export default class NavBar extends Component {

   constructor(props) {
      super(props);

      this.onSelect = this.onSelect.bind(this);
      this.state = {
         selected: Page.BROWSE,
      }
   }

   onSelect(eventKey) {
      this.state.selected = eventKey;
      this.props.onSelect(eventKey);
   }

   render() {
      return (
         <Navbar inverse collapseOnSelect fixedTop onSelect={this.onSelect}>
            <Navbar.Header>
               <Navbar.Brand>
                  <a href="#">Solr Task</a>
               </Navbar.Brand>
               <Navbar.Toggle/>
            </Navbar.Header>
            <Navbar.Collapse>
               <Nav>
                  <NavItem eventKey={Page.BROWSE} active={this.state.selected === Page.BROWSE}
                           href="#">BROWSE</NavItem>
                  <NavItem eventKey={Page.SEARCH} active={this.state.selected === Page.SEARCH}
                           href="#">SEARCH</NavItem>
                  <NavItem eventKey={Page.UPLOAD} active={this.state.selected === Page.UPLOAD}
                           href="#">UPLOAD</NavItem>
                  <NavItem eventKey={Page.CONFIG} active={this.state.selected === Page.CONFIG}
                           href="#">CONFIG</NavItem>
               </Nav>
            </Navbar.Collapse>
         </Navbar>
      );
   }
}