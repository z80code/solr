import React, {Component} from 'react'

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import {defImage} from "../../constants/constants"
import {getImage} from "../../actions/actions";
import imageGif from "../../images/loading.gif";

import {Button, FormControl, FormHelperText, Input, InputLabel, MenuItem, Select} from "material-ui";

const styles = {
   pageTitle: {
      fontSize: "1.8em"
   },
   formControl: {
      fontSize: "0.8em",
      minWidth: 80,
      maxWidth: 120
   },
   suffix:{
      fontSize: "0.6em",
   },
   menuItem: {
      fontSize: "0.8em"
   },
   barInfo: {
      fontSize: "0.8em",
      margin: "0 15px"
   },
   button: {
      margin: "0 15px"
   },
};

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;

const MenuProps = {
   PaperProps: {
      style: {
         maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
         width: 250,
         color: "white"
      },
   },
};

class PageNavigator extends Component {

   constructor(props) {
      super(props);
      this.state = {
         document: null,
         chapterNumber: 0
      };

   }

   componentWillReceiveProps(nextProps) {
      let prop = this.state.properties;
      if (nextProps.document === null) return;
      if (this.state.document === null || this.state.document.id !== nextProps.document.id) {
         this.setState({document: nextProps.document});
      }
   }

   handlePageChange = event => {
      this.setState(
         {
            chapterNumber: event.target.value
         });
      this.props.onChange(event.target.value);
   };

   onBeforeClick = () => {
      if (this.state.chapterNumber > 0) {
         let newNumber = this.state.chapterNumber-1;
         this.setState({
            chapterNumber: newNumber
         });
         this.props.onChange(newNumber);
      }
   };

   onNextClick = () => {
      if (this.state.chapterNumber < this.state.document.chapters.length - 1) {
         let newNumber =this.state.chapterNumber+1;
            this.setState({
            chapterNumber: newNumber
         });
         this.props.onChange(newNumber);
      }
   };

   render() {
      const {classes} = this.props;
      let doc = this.props.document;
      if (doc === null) return null;
      return (
         <div>
            <Button raised className={classes.button} onClick={this.onBeforeClick}>
               Previous page
            </Button>
            <FormControl className={classes.formControl}>
               <Select
                  className={classes.menuItem}
                  value={this.state.chapterNumber}
                  onChange={this.handlePageChange}
                  input={
                     <Input id="name-multiple"/>
                  }
                  MenuProps={MenuProps}>
                  {doc.chapters.map((chapter, i) => (
                     <MenuItem
                        className={classes.menuItem}
                        key={"page_" + i}
                        value={i}
                        style={{
                           fontWeight:
                              this.state.chapterNumber === i
                                 ? "bold"
                                 : "normal",
                        }}>
                        {"Page " + (i + 1)}
                     </MenuItem>
                  ))}
               </Select>
               {/*<FormHelperText className={classes.helpText}>selected page</FormHelperText>*/}
            </FormControl>
            <span className={classes.suffix}> from {doc.chapters.length}</span>
            <Button raised className={classes.button} onClick={this.onNextClick}>
               Next page
            </Button>


         </div>

      );
   }
}

PageNavigator.propTypes = {
   classes: PropTypes.object.isRequired,
   document: PropTypes.object,
   onChange: PropTypes.func.isRequired
};

export default withStyles(styles)(PageNavigator);