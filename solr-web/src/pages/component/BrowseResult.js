import React, {Component} from "react";

import Pagination from "react-js-pagination";
import ViewBook from "./ViewBook"
import ResultFacet from "./ResultFacet";

import imageGif from "../../images/loading.gif";

import {search} from "../../actions/actions";
import BookViewer from "./BookViewer";

const defaultProperties = {
    q: "*:*",
    "fl": "id, title, authors, publisher, annotation, image, creationDate, genres",
    start: 0,
    rows: 20,
    // faceting
    facet: true,
    "facet.field#1": "authors_facets",
    "facet.field#2": "publisher_facets",
    "facet.field#3": "genres_facets",
    "facet.field#4": "creationDate",
    "facet.limit": 10,
};

export default class BrowseResult extends Component {
    constructor(props) {
        super(props);
        this.state = {
            properties: defaultProperties,
            activePage: 1,
            requestResult: null,
            openMessage: false,
            viewDoc: null
        }
    }

    onFacetSelect = (item) => {
        let properties = this.state.properties;
        properties.q = item.field + ':' + item.elem_id;
        this.setState({properties: properties});
        this.searchRequest();
    };

    componentWillReceiveProps(newProps) {
        this.setState({
            requestResult: null
        });
    };

    handlePageChange = (pageNumber) => {
        let prop = this.state.properties;
        prop.start = (+pageNumber - 1) * this.state.properties.rows;
        this.setState({
            properties: prop,
            activePage: pageNumber
        });
        this.searchRequest();
    };

    searchRequest = () => {
        search(this.state.properties.q, this.state.properties, (response) => {
            this.setState({
                openMessage: response.error,
                requestResult: response
            });
        });
    };


    onViewClicked = (doc) => {
        this.setState({viewDoc: doc});
    };

    closeViewer = () => {
        this.setState({viewDoc: null});
    };

    render() {

        if (this.state.requestResult === null) {
            this.searchRequest();
            return (
                <div style={{margin: "0 auto", width: "150px", textAlign: "center"}}>
                    <img src={imageGif} alt="Loading..."/>
                </div>
            );
        }

        if (!this.state.requestResult) return null;

        let requestResult = this.state.requestResult;

        let pagination = "";

        if (requestResult.response.numFound !== 0) {
            pagination = <div style={{textAlign: "center", clear: "both"}}>
                <Pagination activePage={this.state.activePage}
                            itemsCountPerPage={this.state.properties.rows}
                            totalItemsCount={requestResult.response.numFound}
                            pageRangeDisplayed={5}
                            onChange={this.handlePageChange}/>
            </div>;
        }
        return (
            <div style={{clear: "both"}}>
                <ResultFacet fields={requestResult.facet_counts.facet_fields} onSelect={this.onFacetSelect}/>
                <div>
                    {pagination}
                    {
                        requestResult.response.docs.map((doc, i) =>
                            <ViewBook document={doc} key={"doc_result_item_" + i} onView={this.onViewClicked}/>
                        )
                    }
                    {pagination}
                </div>
                <BookViewer document={this.state.viewDoc} onClose={this.closeViewer}/>
            </div>
        );
    }
}