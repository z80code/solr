import React, {Component} from 'react'

import ExpansionPanel, {
   ExpansionPanelDetails,
   ExpansionPanelSummary
} from 'material-ui/ExpansionPanel';

import {
   Button,
   Avatar,
   Grid,
   Divider,
   Chip,
   Typography
} from "material-ui";

import ExpandMoreIcon from 'material-ui-icons/ExpandMore';

const nameSuffix = "_facets";
const undefined = "undefined";

export default class ResultFacet extends Component {

   constructor(props) {
      super(props);
   }

   onClick = (event) => {
      this.props.onSelect(event);
   };

   generateValues=(fields)=> {
      let facets = [];
      for (const key in fields) {
         if (fields.hasOwnProperty(key)) {
            let arr = fields[key].filter((it, j) => !(j % 2))
               .map((item, i, arr) => {
                  return {
                     index: i,
                     id: item,
                     name: !!item.length ? item : undefined,
                     count: fields[key][i * 2 + 1]
                  };
               })
               .filter((it, j) => !!it.count);
            let name = key.charAt(0).toUpperCase() + key.slice(1);
            let totalCount = 0;
            arr.forEach(element => {
               totalCount += +element.count;

            });
            if (totalCount > 99) {
               totalCount = 99;
            }
            facets.push({
               id: name.replace(nameSuffix, "").toLowerCase(),
               name: name.replace(nameSuffix, ""),
               total: totalCount,
               arrayValues: arr
            });
         }
      }
      return facets;
   }

   render() {
      let classes = {};
      let fields = this.props.fields;
      let facets = this.generateValues(fields);

      return (
         <div>
            <ExpansionPanel>
               <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                  <div>
                     <Typography style={{fontSize: "1.2em"}}>Additional results: </Typography>
                  </div>
                  <Grid container spacing={8}>
                     {
                        facets.map((facet, i) => {
                           return <Grid item sm={3} key={"elementF_" + i}>
                              <Chip avatar={
                                 <Avatar style={{
                                    fontSize: "0.6em",
                                    backgroundColor: "#777",
                                    width: "20px",
                                    height: "20px"
                                 }}>
                                    {facet.total + "+"}
                                 </Avatar>}
                                    label={facet.name + " hit(s)"}
                                    onClick={() => {
                                    }}
                                    style={{fontSize: "1em", height: "20px"}}/>
                           </Grid>
                        })
                     }
                  </Grid>

               </ExpansionPanelSummary>
               <ExpansionPanelDetails>
                  <Grid container spacing={0}>
                     <Grid container spacing={0}>
                        {
                           facets.map((facet, i) => {
                              return <Grid item xs={6} sm={3} key={"facet_" + i}>
                                 {facet.name}
                                 <br/>
                                 {
                                    facet.arrayValues.map((element, i) => {
                                       return <Chip key={"element_" + i}
                                                    avatar={<Avatar style={{
                                                       fontSize: "0.8em",
                                                       backgroundColor: "#777"
                                                    }}>{element.count}</Avatar>}
                                                    label={element.name}
                                                    onClick={() => {
                                                       this.onClick({
                                                          field: element.id === "" ? "-" + facet.id : facet.id,
                                                          elem_id: element.id === "" ? '["" TO *]': element.id,
                                                          elem_index: element.index
                                                       })
                                                    }}
                                                    style={{fontSize: "1em"}}
                                       />
                                    })
                                 }
                              </Grid>
                           })
                        }
                     </Grid>
                  </Grid>
               </ExpansionPanelDetails>
               <Divider/>
               <div style={{float: "right"}}>
                  <Grid container spacing={16}>
                     <Grid item xs={12}>
                        <Button onClick={() => {
                           this.onClick({
                              field: "*",
                              elem_id: "*",
                              elem_index: 0
                           })
                        }}>
                           Reset
                        </Button>
                     </Grid>
                  </Grid>
               </div>
            </ExpansionPanel>
         </div>
      );
   }
}