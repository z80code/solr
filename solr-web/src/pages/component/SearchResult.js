import React, {Component} from "react";

import Pagination from "react-js-pagination";
import ResultFacet from "./ResultFacet";
import BookViewer from "./BookViewer";

import imageGif from "../../images/loading.gif";

import {search} from "../../actions/actions";
import {Grid, Paper} from "material-ui";
import FindBookItem from "./FindBookItem";

const requestProperties = {
   // default query options
   "fl": "id, title, authors, publisher, annotation, image, genres",
   "df": "title",
   // Highlighter
   "hl": "on",
   "hl.fl": "title authors publisher annotation chapters genres",
   "hl.simple.pre": '<b style="background: yellow;"><em>',
   "hl.simple.post": '</em></b>',
   // "hl.fragsize": 0,
   // "hl.snippets": 30,
   "hl.preserveMulti": true,
    "hl.MaxAnalyzedChars": 1e6,
   // dismax options
   "defType": "dismax",
   "mm": 10,
   "tie": 0.1,
   "qf": "title^10 authors^5 publisher^3 annotation^2 chapters^1 genres^4",
   // faceting
   "facet": true,
   "facet.field#1": "authors_facets",
   "facet.field#2": "publisher_facets",
   "facet.field#3": "genres_facets",
   "facet.field#4": "creationDate",
   // "facet.limit": 10,
   // pagination info
   "start": 0,
   "rows": 20
};

export default class SearchResult extends Component {
   constructor(props) {
      super(props);
      this.state = {
         properties: requestProperties,
         activePage: 1,
         requestResult: null,
         viewDoc: null,
         searchText: null
      }
   }

   handlePageChange = (pageNumber) => {
      let prop = this.state.properties;
      prop.start = (+pageNumber - 1) * this.state.properties.rows;
      this.setState({
         properties: prop,
         activePage: pageNumber
      });
      this.searchRequest(this.state.searchText);
   };

   searchRequest = (searchText) => {
      if (searchText === "") {
         return;
      }
      search(searchText, this.state.properties, (response) => {
         this.setState({
            requestResult: response
         });
      });
   };

   onFacetSelect = (item) => {
      let properties = this.state.properties;
      properties.fq = item.field + ':' + item.elem_id;
      this.setState({properties: properties});
      this.searchRequest(this.state.searchText);
   };

   onViewClicked = (doc) => {
      this.setState({viewDoc: doc});
   };

   closeViewer = () => {
      this.setState({viewDoc: null});
   };

   componentWillReceiveProps(nextProps) {
      let prop = this.state.properties;
      prop.start = 0;
      this.setState({
         properties: prop,
         searchText: nextProps.searchText,
         activePage: 1
      });
      this.searchRequest(nextProps.searchText);
   }

   render() {

      // if (this.state.requestResult === null) {
      //    this.searchRequest();
      //    return (
      //       <div style={{margin: "0 auto", width: "150px", textAlign: "center"}}>
      //          <img src={imageGif} alt="Loading..."/>
      //       </div>
      //    );
      // }
      if (!this.state.requestResult) return null;

      let requestResult = this.state.requestResult;

      let pagination = "";

      if (requestResult.response.numFound !== 0) {
         pagination = <div style={{textAlign: "center", clear: "both"}}>
            <Pagination activePage={this.state.activePage}
                        itemsCountPerPage={this.state.properties.rows}
                        totalItemsCount={requestResult.response.numFound}
                        pageRangeDisplayed={5}
                        onChange={this.handlePageChange}/>
         </div>;
      }
      return (
         <div style={{clear: "both"}}>
            <ResultFacet fields={requestResult.facet_counts.facet_fields} onSelect={this.onFacetSelect}/>
            <div>
               <Paper style={{margin: "10px 0", padding: "10px"}}
                      id="search_results_summary"
                      onClick={() => {
                      }}>
                  <div>
                     <b>Search results:</b>
                     <Grid container spacing={8}>
                        <Grid item xs={3}><b>Query: </b> {requestResult.responseHeader.params.q}</Grid> {" "}
                        <Grid item xs={3}><b>Time: </b> {requestResult.responseHeader.QTime} ms</Grid> {" "}
                        <Grid item xs={3}><b>Found: </b> {requestResult.response.numFound}</Grid> {" "}
                        <Grid item xs={3}>
                           <b>Pages: </b>
                           {
                              (requestResult.response.numFound % this.state.properties.rows) !== 0 ?
                                 Math.floor(requestResult.response.numFound / this.state.properties.rows) + 1 :
                                 Math.floor(requestResult.response.numFound / this.state.properties.rows)
                           }
                        </Grid>
                     </Grid>
                  </div>
               </Paper>
               {pagination}
               {
                  requestResult.response.docs.map((doc, i) => {
                        doc.highlighting = requestResult.highlighting[doc.id];
                        return <FindBookItem document={doc}
                                             key={"doc_result_item_" + i}
                                             onView={this.onViewClicked}/>
                     }
                  )
               }
               {pagination}
            </div>
            <BookViewer document={this.state.viewDoc} onClose={this.closeViewer} highlight={this.state.searchText}/>
         </div>
      );
   }
}
