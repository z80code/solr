import React, {Component} from 'react'

import {Button, Card, CardContent, Typography} from "material-ui";

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';

import {defImage} from "../../constants/constants"
import {getImage} from "../../actions/actions";
import imageGif from "../../images/loading.gif";

const styles = {
   bookCard: {
      width: "220px",
      float: "left",
      padding: "0 10px",
      margin: "10px 0"
   },
   header: {
      title: {
         fontSize: "0.8em"
      },
      author: {
         fontSize: "0.8em"
      },
      overflow: "hidden",
      height: "72px",
      backgroundColor: "#eee",
      marginBottom: 5
   },
   image: {
      textAlign: "center",
      height: "300px",
      justifyContent: "middle"
   },
   description: {
      height: 110,
      background: "#eee",
      padding: "5px",
      overflow: "hidden"
   },
   openButton: {
      width: "100%"
   }

};


class BookItem extends Component {

   constructor(props) {
      super(props);
      this.state = {
         image: null
      }
   }

   componentWillReceiveProps(nextProps) {
      if (this.props.document.image !== nextProps.document.image) {
         this.setState({image: null});
      }
   }

   updateImage = () => {
      getImage(this.props.document.image,
         (image) => {
            if (image === null) {
               this.setState({image: defImage});
            } else {
               this.setState({
                  image: (image.indexOf("data") === 0 ? "" : 'data:image/jpeg;base64, ') + image
               });
            }
         }
      );
   };

   openViewBook = () => {
      let doc = this.props.document.highlighting || this.props.document;
      doc.base64image = this.state.image;
      doc.id=this.props.document.id;
      this.props.onView(doc);
   };

   render() {
      const {classes} = this.props;

      let doc = this.props.document;
      if (this.state.image === null) {
         this.updateImage();
      }

      return (
         <div className={classes.bookCard}>
            <Card>
               <div>
                  <CardContent className={classes.header}>
                     <Typography type="headline" className={classes.header.title}> {doc.title}</Typography>
                     <Typography type="subheading" color="secondary" className={classes.header.author}>
                        {doc.authors.join(", ")}
                     </Typography>
                  </CardContent>
                  <div className={classes.image}>
                     <img alt={doc.title} title={doc.title} src={this.state.image || imageGif}
                          style={{width: "90%", margin: "0 auto"}}/>
                  </div>
                  {
                     this.props.content || null
                  }
                  <Button color="default" onClick={this.openViewBook} className={classes.openButton}>
                     View
                  </Button>
               </div>
            </Card>
         </div>
      );
   }
}


BookItem.propTypes = {
   classes: PropTypes.object.isRequired,
   content: PropTypes.element.isRequired,
   onView: PropTypes.func.isRequired,
   document: PropTypes.object.isRequired
};

export default withStyles(styles)(BookItem);