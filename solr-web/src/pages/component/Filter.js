import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {FormGroup, FormControlLabel} from 'material-ui/Form';
import Checkbox from 'material-ui/Checkbox';

const styles = {};

class Filter extends React.Component {
   state = {
      checkedAll: true,
      checkedTitle: true,
      checkedAuthor: true,
      checkedPublisher: true,
      checkedGenre: true,
      extendSearch: false
   };

   generateList = (state) => {
      let result = [];
      if (state.checkedAll) {
         return result;
      }
      if (state.checkedAuthor) {
         result.push("authors");
      }
      if (state.checkedTitle) {
         result.push("title");
      }
      if (state.checkedPublisher) {
         result.push("publisher");
      }
      if (state.checkedGenre) {
         result.push("genres");
      }
      return result;
   };

   handleAllChange = (event) => {
      // let {checked} = event.target;
      this.setState({
         checkedAll: true,
         checkedTitle: true,
         checkedAuthor: true,
         checkedPublisher: true,
         checkedGenre: true,
      });
      this.props.onSelest(
         {fields: []}
      );
   };


   handleChange = name => event => {
      let {checked} = event.target;

      let state = this.state;
      state[name] = checked;

      state["checkedAll"] = (state.checkedTitle && state.checkedAuthor &&
         state.checkedPublisher && state.checkedGenre);

      this.setState({
         checkedAll: state.checkedAll,
         [name]: checked
      });

      this.props.onSelest(
         {fields: this.generateList(state)}
      );
   };

   render() {
      const {classes} = this.props;
      return (
         <FormGroup row>
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.checkedAll}
                     onChange={this.handleAllChange}
                     value="all"
                  />
               }
               label="Anywhere"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.checkedTitle}
                     onChange={this.handleChange('checkedTitle')}
                     value="title"
                  />
               }
               label="by Title"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.checkedAuthor}
                     onChange={this.handleChange('checkedAuthor')}
                     value="authors"
                  />
               }
               label="by Author"
            />
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.checkedPublisher}
                     onChange={this.handleChange('checkedPublisher')}
                     value="publisher"
                  />
               }
               label="by Publisher"/>
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.checkedGenre}
                     onChange={this.handleChange('checkedGenre')}
                     value="genres"
                  />
               }
               label="by Genre"/>
            <FormControlLabel
               control={
                  <Checkbox
                     checked={this.state.extendSearch}
                     onChange={this.handleChange('extendSearch')}
                     value="extend"
                  />
               }
               label="extend Search"/>
         </FormGroup>
      );
   }
}

Filter.propTypes = {
   classes: PropTypes.object.isRequired,
   onSelest: PropTypes.func.isRequired
};

export default withStyles(styles)(Filter);