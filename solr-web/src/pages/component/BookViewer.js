import React, {Component} from 'react'

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import Button from 'material-ui/Button';
import Dialog from 'material-ui/Dialog';
import AppBar from 'material-ui/AppBar';
import Toolbar from 'material-ui/Toolbar';
import Typography from 'material-ui/Typography';
import Slide from 'material-ui/transitions/Slide';

import {bookStyle} from "./css/formatingBook.css"
import $ from "jquery";

import {defImage} from "../../constants/constants"
import {getImage, search} from "../../actions/actions";
import imageGif from "../../images/loading.gif";

import {Grid, Paper} from "material-ui";
import PageNavigator from "./PageNavigator";

const styles = {
   appBar: {
      position: 'relative',
      backgroundColor: "#eee",
      color: "#333",
      fontSize: "2em"
   },
   title: {
      fontSize: "1.5em",
      fontWeight: "bold"
   },
   author: {
      fontSize: "1.0em",
      fontWeight: "bold"
   },
   flex: {
      flex: 1,

   },
   page: {
      overflowY: "auto",
      margin: "0px auto 5px",
      overflowX: "hidden",
      padding: "40px"
   },
   root: {
      fontSize: "2em",
      padding: 16,
      paddingBottom: 16,
      marginTop: "48px auto",
   },
   pageTitle: {
      fontSize: "1.5em"
   },
   pageContent: {
      fontSize: "1em"
   },
   container: {
      display: 'flex',
      flexWrap: 'wrap',
   }
};

function Transition(props) {
   return <Slide direction="up" {...props} />;
}

const requestProperties = {
   // default query options
   "fl": "id, title, authors, publisher, annotation, image, genres, chapters",
   "df": "title",
   // Highlighter
   "hl": "off",
   "hl.fl": "title authors publisher annotation chapters genres",
   "hl.simple.pre": '<b style="background: yellow;"><em>',
   "hl.simple.post": '</em></b>',
   "hl.fragsize": 0,
   "hl.snippets": 30,
   "hl.preserveMulti": true,
   "hl.MaxAnalyzedChars": 100000,
   "start": 0,
   "rows": 5
};

class BookViewer extends Component {

   constructor(props) {
      super(props);
      this.state = {
         open: false,
         image: null,
         document: null,
         chapterNumber: 0,
         properties: requestProperties
      };
   }

   componentWillReceiveProps(nextProps) {
      const {document} = nextProps;
      if (document === null) return;
      if (this.state.document === null || document.id !== this.state.document.it) {
         this.setState(
            {
               open: true,
               image: null,
               document: null,
               chapterNumber: 0
            });
         this.searchRequest(nextProps.document.id, nextProps.highlight);
      }
   }

   searchRequest = (id, searchText) => {
      if (searchText === "") {
         return;
      }
      let properties = this.state.properties;
      let qString = "id:\"" + id + "\"";
      if (!!searchText) {
         properties["hl.q"] = searchText;
         properties["hl"] = "on";
      }
      search(qString, properties, (response) => {
            let doc = null;
            if (!!this.props.highlight) {
               doc = response.highlighting[this.props.document.id];
            } else {
               doc = response.response.docs[0];
            }
            let strImage = "<img src=\"" + this.props.document.base64image + "\"alt=\"\"/>";
            doc.chapters.unshift(strImage);
            this.setState({
               document: doc
            });
         }
      );
   };


   handleClose = () => {
      this.setState({open: false});
      this.props.onClose();
   };

   handlePageChange = pageNumber => {
      $('#beginOfPage').scrollTop(0);
      this.setState({chapterNumber: pageNumber});
   };


   render() {
      const {classes} = this.props;
      let doc = this.state.document;
      if (doc === null){
         return null;
      }

      return (
         <div>
            <Dialog
               fullScreen
               open={this.state.open}
               onClose={this.handleClose}
               transition={Transition}>
               <AppBar className={classes.appBar}>
                  <Toolbar>
                     <Typography type="title" color="inherit" className={classes.flex}>
                        Book preview
                     </Typography>
                     <Typography type="title" color="inherit" className={classes.flex}>
                        <div className={classes.title} dangerouslySetInnerHTML={{__html: doc.title}}/>
                        <div>
                           by {" "}
                           <span className={classes.author}
                                dangerouslySetInnerHTML={{__html: doc.authors.join(", ")}}/>
                        </div>
                     </Typography>
                     <PageNavigator document={doc} onChange={this.handlePageChange}/>
                     <Button raised onClick={this.handleClose}>
                        Close
                     </Button>
                  </Toolbar>
               </AppBar>
               <div className={classes.page} id="beginOfPage">
                  <Grid container spacing={40}>
                     <Paper className={classes.root} elevation={4}>
                        <Typography type="headline" component="h3" className={classes.pageTitle}>
                           {/*{"Chapter " + (+this.state.chapterNumber + 1)}*/}
                        </Typography>
                        <Typography component="div" className={classes.pageContent}>
                           <div dangerouslySetInnerHTML={{__html: doc.chapters[this.state.chapterNumber]}}/>
                        </Typography>
                     </Paper>
                  </Grid>
               </div>
            </Dialog>
         </div>
      );
   }
}

BookViewer.propTypes = {
   classes: PropTypes.object.isRequired,
   highlight: PropTypes.string
};

export default withStyles(styles)(BookViewer);