import React, {Component} from "react";

import Button from "react-bootstrap/es/Button";

import {clean, config} from "../actions/actions";

export default class Config extends Component {

    onClickConfigResult = (result) => {
       console.log(result);
    };
    onClickCleanResult = (result) => {
       console.log(result);
    };

    render() {
        const wellStyles = {maxWidth: 400, margin: '0 auto 10px'};
        return (
            <div>
                <h1 style={{textAlign: "center"}}>Cleaning index.</h1>
                <div className="well" style={wellStyles}>
                    <Button bsStyle="primary" bsSize="large" block
                            onClick={config((result) => {
                                this.onClickConfigResult(result)
                            })}
                    >Configurable ON</Button>
                    <Button bsStyle="primary" bsSize="large" block
                            onClick={clean((result) => {
                                this.onClickCleanResult(result)
                            })}
                    >Clean</Button>
                </div>
            </div>
        );
    }
}
