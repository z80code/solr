import React, {Component} from "react";

import SearchResult from "./component/SearchResult";
import IntegrationAutosuggest from "./component/SearchInput"

import PropTypes from 'prop-types';
import {withStyles} from 'material-ui/styles';
import {Button, Grid} from "material-ui";
import Filter from "./component/Filter";

const styles = {};

class Search extends Component {

   constructor(props) {
      super(props);
      this.searchText = "";
      this.filters = [];
      this.state = {
         searchValue: ""
      }
   }

   handleSearchButton = () => {
      this.setState({
         searchValue: this.searchText
      });
   };

   valueUpdated = (valueNew) => {
      this.searchText = valueNew;
   };

   handleUpdateFilters = (filters) => {
      this.filters = filters;
   };

   render() {
      return (
         <div>
            <div style={{display: "block", margin: "0 auto", width: 800}}>
               <Grid container spacing={8}>
                  <Grid item xs={12} style={{textAlign: "center"}}>
                     <h1>Search</h1>
                     <h5>of nothing..</h5>
                  </Grid>
                  <Grid item xs={12}>
                     {/*<Filter onSelest={this.handleUpdateFilters}/>*/}
                  </Grid>
                  <Grid item xs={10}>
                     <IntegrationAutosuggest valueUpdated={this.valueUpdated}/>
                  </Grid>
                  <Grid item xs={2}>
                     <Button raised color="primary" onClick={this.handleSearchButton}>
                        Search
                     </Button>
                  </Grid>
               </Grid>

            </div>
            <SearchResult searchText={this.state.searchValue} filters={this.filters}/>
         </div>

      )
         ;
   }
}

Search.propTypes = {
   classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Search);