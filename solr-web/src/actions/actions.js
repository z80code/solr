import {Properties} from "../constants/constants"

import $ from "jquery";

function getSuggest(queryString, callBack) {
   if (queryString.length===0) return;
   let papamsString = "?suggest=true&suggest.dictionary=libSuggester&suggest.q=";
   $.ajax(
      {
         url: Properties.END_POINT_SOLR_SUGGEST + papamsString + queryString,
         type: "GET",
         dataType: "json",
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(result);
         }
      });
}

function search(queryString, properties, callBack) {
   let papamsString = 'q=' + queryString + '&wt=json';

   for (let key in properties) {
      papamsString += "&" + key.replace(/#(\d)+/, "") + "=" + encodeURI(properties[key]);
   }
   $.ajax(
      {
         url: Properties.END_POINT_SOLR_SEARCH + "?" + papamsString,
         type: "GET",
         // data: properties,
         dataType: "json",
         // data: queryString,
         success: (result) => {
            callBack(result);
         },
         error: (result, e) => {
            result.error = e;
            callBack(result);
         }
      });
}

function send(formData, callBack) {
   $.ajax(
      {
         url: Properties.END_POINT_UPLOAD,
         type: 'POST',
         data: formData,
         processData: false,
         contentType: false,
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(result);
         }
      });
}

function clean(callBack) {
   // -ContentType "application/json"
   $.ajax(
      {
         url: "http://localhost:8983/solr/library/update?stream.body=%3Cdelete%3E%3Cquery%3E*:*%3C/query%3E%3C/delete%3E&commit=true",
         type: 'GET',
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(result);
         }
      });
}

function config(callBack) {
   // -ContentType "application/json"
   $.ajax(
      {
         url: "http://localhost:8983/solr/library/config",
         type: 'POST',
         data: '{"set-property" : {"requestDispatcher.requestParsers.enableRemoteStreaming":true},"set-property" : {"requestDispatcher.requestParsers.enableStreamBody":true}}',
         dataType: "json",
         processData: false,
         contentType: false,
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(result);
         }
      });
}

function getImage(fileName, callBack) {
   $.ajax(
      {
         url: Properties.END_POINT_RESOURCE_IMAGE + fileName,
         type: "GET",
         // data: properties,
         dataType: "text",
         // data: queryString,
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(null);
         }
      });
}

function getFile(fileName, callBack) {
   $.ajax(
      {
         url: Properties.END_POINT_RESOURCE_FILE + "/" + fileName,
         type: "GET",
         // data: properties,
         dataType: "text",
         // data: queryString,
         success: (result) => {
            callBack(result);
         },
         error: (result) => {
            callBack(result);
         }
      });
}


export {search, send, clean, config, getImage, getFile, getSuggest};