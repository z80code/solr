package by.epam.training.service;

import by.epam.training.service.exception.SolrServiceException;

import java.io.IOException;
import java.util.Collection;

public interface ISolrService<T> {
	void search(String string) throws SolrServiceException;

	void index(Collection<T> books) throws SolrServiceException;
}
