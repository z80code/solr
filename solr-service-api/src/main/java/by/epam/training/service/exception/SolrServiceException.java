package by.epam.training.service.exception;

public class SolrServiceException extends Exception {
	public SolrServiceException() {
	}

	public SolrServiceException(String message) {
		super(message);
	}

	public SolrServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
