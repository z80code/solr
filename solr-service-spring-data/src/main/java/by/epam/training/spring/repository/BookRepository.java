//package by.epam.training.spring.repository;
//
//import by.epam.training.entity.BookDocument;
//import org.springframework.data.solr.repository.SolrCrudRepository;
//
//import java.util.List;
//
//public interface BookRepository extends SolrCrudRepository<BookDocument, String> {
//
//	List<BookDocument> findByName(String name);
//}
