package by.epam.training.storage.impl;

import by.epam.training.storage.IFileStorage;
import by.epam.training.storage.exception.FileStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class FileStorage implements IFileStorage {

   private static final String FILE_UPLOADING_DIR = System.getProperty("user.dir") + "/uploadingdir/";
   private static final String IMAGE_UPLOADING_DIR = System.getProperty("user.dir") + "/uploadingdir/images/";

   private Logger logger = LoggerFactory.getLogger(FileStorage.class);

   synchronized boolean createDir(File file) {
      if (!file.exists()) {
         return file.mkdir();
      } else {
         return true;
      }
   }

   public FileStorage() throws FileStorageException {
      File fileDir = new File(FILE_UPLOADING_DIR);
      File imageDir = new File(IMAGE_UPLOADING_DIR);

      if (!createDir(fileDir)) {
         logger.error("File storage dir are not created.");
         throw new FileStorageException("File storage dir are not created.");
      }
      logger.info("File storage dir was created.");
      if (!createDir(imageDir)) {
         logger.error("Image storage dir are not created.");
         throw new FileStorageException("Image storage dir are not created.");
      }
      logger.info("Image storage dir was created.");
   }

   @Override
   synchronized public File store(MultipartFile uploadedFile) throws FileStorageException {
      File file = new File(FILE_UPLOADING_DIR + uploadedFile.getOriginalFilename());
      if (file.exists()) {
         return file;
      }
      try {
         logger.info("Creating file with name: " + uploadedFile.getOriginalFilename());
         uploadedFile.transferTo(file);
         logger.info("Created file with name: " + uploadedFile.getOriginalFilename());
         return file;
      } catch (IOException e) {
         logger.error("" +
                 "This file can`t be created: "+ uploadedFile.getOriginalFilename());
         throw new FileStorageException("File can`t be created.", e);
      }
   }

   private final static String SUFFIX_FILE = ".fb2";

   @Override
   synchronized public File getByNameFile(String fileName) throws FileStorageException {
      logger.info("Reading file with name: " + fileName);
      return new File(FILE_UPLOADING_DIR + fileName + SUFFIX_FILE);
   }

   private final static String SUFFIX_IMAGE = ".img";

   @Override
   synchronized public void storeImageBase64(String resourceName, String content) throws FileStorageException {
      logger.info("Storing file with name: " + resourceName);
      BufferedWriter writer = null;
      try {
         writer = new BufferedWriter(new FileWriter(IMAGE_UPLOADING_DIR + resourceName + SUFFIX_IMAGE));
         writer.write(content);
         logger.info("Storing success file with name: " + resourceName);
      } catch (IOException e) {
         logger.error("Error storing image with name: " + resourceName);
         throw new FileStorageException("File can`t be created.", e);
      } finally {
         try {
            if (writer != null)
               writer.close();
         } catch (IOException e) {
            logger.error("File can`t be closed: " + resourceName);
            throw new FileStorageException("File can`t be closed.", e);
         }
      }
   }

   private static final String CHARSET_ENCODE = "UTF-8";

   @Override
   synchronized public String getImageBase64ByName(String fileName) throws FileStorageException {
      logger.info("Reading file with name: " + fileName);
      try {
         byte[] encoded = Files.readAllBytes(Paths.get(IMAGE_UPLOADING_DIR + fileName + SUFFIX_IMAGE));
         logger.info("Reading success file with name: " + fileName);
         return new String(encoded, CHARSET_ENCODE);
      } catch (IOException e) {
         logger.error("File can`t be read: " + fileName);
         throw new FileStorageException("File can`t be read.", e);
      }
   }
}
