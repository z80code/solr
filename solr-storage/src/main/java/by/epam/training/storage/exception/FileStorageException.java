package by.epam.training.storage.exception;

public class FileStorageException extends Exception {
	public FileStorageException() {
	}

	public FileStorageException(String message) {
		super(message);
	}

	public FileStorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
