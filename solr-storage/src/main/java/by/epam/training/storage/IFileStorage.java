package by.epam.training.storage;

import by.epam.training.storage.exception.FileStorageException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

public interface IFileStorage {
   File store(MultipartFile uploadedFile) throws FileStorageException;

   File getByNameFile(String fileName) throws FileStorageException;

   void storeImageBase64(String fileName, String content) throws FileStorageException;

   String getImageBase64ByName(String fileName) throws FileStorageException;
}
